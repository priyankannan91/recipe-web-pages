from django.db import models
from django.conf import settings

# USER_MODEL = settings.AUTH_USER_MODEL
# Replaced with settings.AUTH_USER_MODEL (See Recipe.author below)

# Create your models here.
class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    rating = models.CharField(max_length=10)
    created_on = models.DateTimeField(auto_now_add=True)

    # 1 author:N recipes
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True,
    )
    #How does this update Recipe (under Recipe steps in admin) to populate with title
    def __str__(self):
        return self.title

#Adding 'steps' to recipe page
class RecipeStep(models.Model):
    step_number = models.PositiveSmallIntegerField()
    instruction = models.TextField()

    recipe = models.ForeignKey(
        Recipe,
        related_name="steps",
        on_delete=models.CASCADE,
    )
    class Meta:
        ordering = ["step_number"]

#Adding 'ingredients' to recipe page
class RecipeIngredient(models.Model):
    amount = models.CharField(max_length=100)
    food_item = models.CharField(max_length=100)
    recipe = models.ForeignKey(
        Recipe,
        related_name="ingredients",
        on_delete=models.CASCADE,
    )
    class Meta:
        ordering = ["food_item"]
