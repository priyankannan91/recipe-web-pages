# Generated by Django 4.2.2 on 2023-07-11 19:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0008_recipeingredient'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='recipeingredient',
            options={'ordering': ['food_item']},
        ),
    ]
