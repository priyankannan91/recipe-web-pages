from django.forms import ModelForm
from .models import Recipe

#Will be useful for Create and Edit Views

#Class will inherit from ModelForm
class RecipeForm(ModelForm):
    #Nested Class, Does this have to be called 'Meta'?
    class Meta:
        #Work with Recipe model
        model = Recipe
        #Fiels to display
        fields = [
            'title',
            'picture',
            'rating'
        ]
