from django.shortcuts import render, get_object_or_404, redirect
from .models import Recipe
from .forms import RecipeForm
from django.contrib.auth.decorators import login_required

# Create your views here.
# SINGLE object view
def show_recipe(request, id):
    ##WHAT DOES 'Recipe' DO?##
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/each_recipe.html", context)

# ALL objects view
def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)

#Filter for user's entries/recipes
#user must be logged in to view self published recipes
@login_required
def my_recipe_list(request):
    #filter for recipes where author matches the user logged in
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)

# Validate submission and redirect, else create instance of RecipeForm
def create_recipe(request):
    if request.method == "POST":
        #form will hold the submitted recipe
        form = RecipeForm(request.POST)
        if form.is_valid():
            #this form will save the recipe, but not to database
            recipe = form.save(False)
            #assign current user to author property
            recipe.author = request.user
            #save recipe to database
            form.save()
            #page containing all the recipes
            return redirect(recipe_list)
    else:
        #form will hold the instance
        form = RecipeForm()

    # @login_required(login_url="user_login")
    # def my_view(request):
    #     return redirect("recipe_list")

    context = {
        "form": form
    }
    return render(request, "recipes/create.html", context)


# Edit Posts
def edit_recipe(request, id):
    #getting recipe
    recipe = Recipe.objects.get(id=id)
    #
    if request.method == "POST":
        #instance=post updates
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            #Saves to database
            recipe = form.save()
            return redirect("show_recipe", id=id)
    else:

        form = RecipeForm(instance=recipe)

    context = {
        "recipe_object": recipe,
        "form": form,
    }
    return render(request, "recipes/edit.html", context)
