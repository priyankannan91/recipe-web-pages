from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from accounts.models import SignUpForm, LoginForm

# Create your views here.

#Define Login Registration Form (in accounts.models)
def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        #if form is not valid, then clean_data will be empty
        if form.is_valid():
            #Unlike models this form will not be saved to the database
            #Stores all the valid data in form.cleaned_data dictionary for retreival
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            first_name = form.cleaned_data["first_name"]
            last_name = form.cleaned_data["last_name"]
            #confirm password matches
            if password == password_confirmation:
              # Create new login and save information to properties
              user = User.objects.create_user(
                  username,
                  password=password,
                  first_name=first_name,
                  last_name=last_name,
              )
              # Log in user and redirect to main page
              login(request, user)
              return redirect("recipe_list")
            #if passwords do not match return error message
            else:
                form.add_error("password", "Passwords do not match")
    else:
        form = SignUpForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)


#Define Login
#GET - show the form
#POST - try to log person in
def user_login(request):
    #??
    if request.method == "POST":
        #??
        form = LoginForm(request.POST)
        #if form is valid
        if form.is_valid():
            #save username and password
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            #Confirm if request username and password match database
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            #If user(username+password combo) exists in database
            if user is not None:
                #Login and redirect to homepage
                login(request, user)
                return redirect("recipe_list")
            #If user does not exist in database
            else:
                form.add_error("username", "Invalid Login")
    #create a blank instance of LoginForm
    else:
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)

#Logout
def user_logout(request):
    logout(request)
    return redirect("recipe_list")
